<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class WebmailSent extends Model
{
    protected $table = 'webmail_sent';
}

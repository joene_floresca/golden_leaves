<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Form;
use App\Http\Models\Form2;
use App\Http\Models\Number;
use Validator;
use Input;
use Redirect;
use Session;
use Auth;
use Response;
use DB;
use Excel;

class ReportController extends Controller
{
  public function showEosrReport()
  {
    return view('reports.eosr');
  }

  public function getEosrReport()
  {
    $from  = Input::get('from');
    $to    = Input::get('to');
    $query = "SELECT b.name,
    COUNT(CASE WHEN a.updated_action = 'Pack-out Mail' then a.agent_id end) as packoutmail ,
    COUNT(CASE WHEN a.updated_action = 'Pack-out Email' then a.agent_id end) as packoutemail,
    COUNT(CASE WHEN a.updated_action = 'Callback' then a.agent_id end) as callback,
    COUNT(CASE WHEN a.status = 'Sale' then a.agent_id end) as sale ,
    COUNT(CASE WHEN a.status = 'Rejected' then a.agent_id end) as rejected
        FROM forms a INNER JOIN users b ON a.agent_id = b.id
        WHERE input_date >= '$from ' AND input_date <= '$to' GROUP BY a.agent_id;";

    $data = DB::connection('mysql')->select($query);
    return json_encode($data);
  }

  public function showReport1()
  {
   
    $agent_options = array('' => 'Choose One') + DB::table('users')->lists('name','id');
    return view('reports.report1')->with(array('agent_options'=>$agent_options));

  }

  public function getReport1()
  {
    $from = Input::get('from');
    $to   = Input::get('to');
    $agent_id = Input::get('agent_id');

    if($agent_id == "")
    {
      $data = Form::where('input_date', '>=', $from)->where('input_date', '<=', $to)->get();
    }
    else
    {
      $data = Form::where('input_date', '>=', $from)->where('input_date', '<=', $to)->where('agent_id', '=', $agent_id)->get();
    }
    
    Excel::create('Filename', function($excel) use($data) {
        $excel->sheet('Sheetname', function($sheet) use($data) {
            $sheet->fromArray($data);
        });
    })->export('xls');
  }
}

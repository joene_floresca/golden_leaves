<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Response;
use Validator;
use Mail;
use Redirect;
use Session;
use URL;
use App\Http\Models\WebmailSent;
use Datatables;
use Auth;

class PackoutController extends Controller
{
    public function showPackoutEmail()
    {
        return view('packout.packout-email');
    }

    public function processPackoutEmail()
    {
        $rules = array(
            'email_from'         => 'required',
            'email_to'           => 'required',
            'packout_type'       => 'required',
           // 'package_attachment' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::to('packout-email')->withInput()->withErrors($validator);
        }
        else
        {
            if(Input::get('packout_type') == "A")
            {
                $path = asset('/').'docs/From50_brochure.pdf'; 
            }
            else if(Input::get('packout_type') == "B")
            {
                switch(Input::get('package_attachment'))
                {
                    case "Silver":
                        $path = asset('/').'docs/Silver_Letter.docx'; 
                    break;
                    case "Platinum":
                        $path = asset('/').'docs/Platinum_Letter.docx'; 
                    break;
                    case "Gold":
                        $path = asset('/').'docs/Gold_Letter.docx'; 
                    break;
                    default:
                }
            }

            $to = Input::get('email_to');
            $body = Input::get('editor1');
            Mail::raw($body, function ($m) use ($body, $path, $to) {
                $m->from('info@futurechoices.co.uk', 'Future Choices');
                $m->to($to, '')->subject('Greetings from Future Choices!');
                $m->setBody($body, 'text/html');
                $m->attach($path);
            });

            $date = date_create();
            $date->setTimezone(new \DateTimeZone('Asia/Taipei'));
            $sent = new WebmailSent();
            $sent->from_email     = Input::get('email_from');
            $sent->to_email       = Input::get('email_to');
            $sent->packout_type   = Input::get('packout_type');
            $sent->package_type   = Input::get('package_attachment');
            $sent->ph_timesent    = date_format($date, 'Y-m-d H:i:s');
            $sent->sent_by_user_id  = Auth::user()->id;
            $sent->save();

            Session::flash('alert-success', 'Email sent.');
            return Redirect::to('packout-email');
        }
    }

    public function showWebmailSent()
    {
        return view('packout.webmail-sent');
    }

    public function ajaxWebmailSent()
    {
        $webmail = WebmailSent::leftJoin('users', 'webmail_sent.sent_by_user_id', '=', 'users.id')
           // ->select(['posts.id', 'posts.title', 'users.name', 'users.email', 'posts.created_at', 'posts.updated_at']);
            ->select(['webmail_sent.id', 'webmail_sent.from_email', 'webmail_sent.to_email', 'webmail_sent.packout_type', 'webmail_sent.package_type', 'webmail_sent.created_at', 'webmail_sent.ph_timesent', 'users.name']);

       // $webmail = WebmailSent::(select['id','from_email','to_email','packout_type','package_type', 'created_at', 'ph_timesent']);
        return Datatables::of($webmail)->make();


    }

}

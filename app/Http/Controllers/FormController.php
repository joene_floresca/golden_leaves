<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Form;
use App\Http\Models\Form2;
use App\Http\Models\Number;
use Validator;
use Input;
use Redirect;
use Session;
use Auth;
use Response;
use DB;
use Datatables;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
          if(Auth::user()->access_level == 0 || Auth::user()->access_level == 3)
          {
            //$agent_options = array('' => 'Choose One') + DB::table('users')->where('Status', '1')->lists('id','name');
            $agent_options = array('' => 'Choose One') + DB::table('users')->lists('name','id');
            return view('form.index')->with(array('agent_options'=>$agent_options));
          }
          else
          {
              return Response::view('errors.404', array(), 404);
          }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::check())
      {
        if(Auth::user()->access_level == 0 || Auth::user()->access_level == 3)
        {
          $number = Number::all();
          $current = $number[0]->number;
          $newval = $current + 1;
          Number::where('id', '=', 1)->update(['number' => $newval]);
          return view('form.create')->with('number', $current);
        }
        else
        {
            return Response::view('errors.404', array(), 404);
        }
      }
      else
      {
        return Redirect::to('auth/login');
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //date_default_timezone_set("Europe/London");
      $current_time = date('Y-m-d h:i:s');
      // Save to temp table without any validation
      $form_temp = new Form2();
      $form_temp->form_num                     = Input::get('form_num');
      $form_temp->input_date                   = $current_time;
      $form_temp->plan_holder                  = Input::get('plan_holder');
      $form_temp->birth_date                   = Input::get('birth_date');
      $form_temp->address                      = Input::get('address');
      $form_temp->postcode                     = Input::get('postcode');
      $form_temp->telephone                    = Input::get('telephone');
      $form_temp->plan_applicant               = Input::get('plan_applicant');
      $form_temp->applicant_relationship       = Input::get('applicant_relationship');
      $form_temp->app_rel_address              = Input::get('app_rel_address');
      $form_temp->app_rel_postcode             = Input::get('app_rel_postcode');
      $form_temp->app_rel_tel                  = Input::get('app_rel_tel');
      $form_temp->app_rel_email                = Input::get('app_rel_email');
      $form_temp->is_notified_arrangement      = Input::get('is_notified_arrangement');
      $form_temp->executors                    = Input::get('executors');
      $form_temp->executors_tel                = Input::get('executors_tel');
      $form_temp->executors_address            = Input::get('executors_address');
      $form_temp->funeral_type                 = Input::get('funeral_type');
      $form_temp->funeral_plan_type                 = Input::get('funeral_plan_type');
      $form_temp->funeral_details_notes               = Input::get('funeral_details_notes');
      $form_temp->payment_option                      = Input::get('payment_option');
      $form_temp->payment_full_type_selected          = Input::get('payment_full_selected');
      $form_temp->payment_full_amount_selected        = Input::get('payment_full_amount_selected');
      $form_temp->payment_installment_type_selected   = Input::get('payment_installment_type_selected');
      $form_temp->payment_installment_amount_selected = Input::get('payment_installment_amount_selected');
      $form_temp->payment_fixed_age_selected          = Input::get('payment_fixed_age_selected');
      $form_temp->payment_fixed_type_selected         = Input::get('payment_fixed_type_selected');
      $form_temp->payment_fixed_monthly_selected      = Input::get('payment_fixed_monthly_selected');
      $form_temp->action                              = Input::get('action');
      $form_temp->success_transfer                    = Input::get('success_transfer');
      $form_temp->unsuccess_transfer                  = Input::get('unsuccess_transfer');
      $form_temp->planholder_email                    = Input::get('planholder_email');
      $form_temp->agent_special_note                    = Input::get('agent_special_note');
      $form_temp->agent_id                            = Auth::user()->id;
      $form_temp->save();

      // Saving with validations

      $rules = array(
          //'input_date'             => 'required',
          'plan_holder'            => 'required',
        //  'birth_date'             => 'required',
        //  'address'                => 'required',
          'postcode'               => 'required',
          'telephone'              => 'required',
        //  'funeral_type'           => 'required',
        //  'funeral_plan_type'      => 'required',
        //  'payment_option'         => 'required',
      );

      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails())
      {
          return Redirect::to('form/create')->withErrors($validator);
      }
      else
      {
        $form = new Form();
        $form->form_num                     = Input::get('form_num');
        $form->input_date                   = $current_time;
        $form->plan_holder                  = Input::get('plan_holder');
        $form->birth_date                   = Input::get('birth_date');
        $form->address                      = Input::get('address');
        $form->postcode                     = Input::get('postcode');
        $form->telephone                    = Input::get('telephone');
        $form->plan_applicant               = Input::get('plan_applicant');
        $form->applicant_relationship       = Input::get('applicant_relationship');
        $form->app_rel_address              = Input::get('app_rel_address');
        $form->app_rel_postcode             = Input::get('app_rel_postcode');
        $form->app_rel_tel                  = Input::get('app_rel_tel');
        $form->app_rel_email                = Input::get('app_rel_email');
        $form->is_notified_arrangement      = Input::get('is_notified_arrangement');
        $form->executors                    = Input::get('executors');
        $form->executors_tel                = Input::get('executors_tel');
        $form->executors_address            = Input::get('executors_address');
        $form->funeral_type                 = Input::get('funeral_type');
        $form->funeral_plan_type                 = Input::get('funeral_plan_type');
        $form->funeral_details_notes               = Input::get('funeral_details_notes');
        $form->payment_option                      = Input::get('payment_option');
        $form->payment_full_type_selected          = Input::get('payment_full_selected');
        $form->payment_full_amount_selected        = Input::get('payment_full_amount_selected');
        $form->payment_installment_type_selected   = Input::get('payment_installment_type_selected');
        $form->payment_installment_amount_selected = Input::get('payment_installment_amount_selected');
        $form->payment_fixed_age_selected          = Input::get('payment_fixed_age_selected');
        $form->payment_fixed_type_selected         = Input::get('payment_fixed_type_selected');
        $form->payment_fixed_monthly_selected      = Input::get('payment_fixed_monthly_selected');
        $form->action                              = Input::get('action');
        $form->success_transfer                    = Input::get('success_transfer');
        $form->unsuccess_transfer                  = Input::get('unsuccess_transfer');
        $form->planholder_email                   = Input::get('planholder_email');
        $form->agent_special_note                   = Input::get('agent_special_note');
        $form->agent_id                            = Auth::user()->id;


        if($form->save())
        {
          $newval = intval(Input::get('form_num')) + 1;
          Session::flash('alert-success', 'Form submitted successfully.');
          Number::where('id', '=', 1)->update(['number' => $newval]);
        }
        else
        {
          Session::flash('alert-danger', 'Error submitting form. Please try again');
        }

        return Redirect::to('form/create');

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showViewForm()
    {
      if(Auth::check())
      {
        if(Auth::user()->access_level == 1 || Auth::user()->access_level == 3 || Auth::user()->access_level == 0)
        {
          return view('form.view-form');
        }
        else
        {
            return Response::view('errors.404', array(), 404);
        }
      }
      else
      {
        return Redirect::to('auth/login');
      }

    }

    public function searchFormID()
    {
      $data = array();
      $data2 = array();
      $id = Input::get("id");
      //$form = Form::where('form_num', $id )->get();
      $query = "SELECT * FROM forms2 a INNER JOIN users b ON a.agent_id = b.id WHERE a.form_num = '$id';";
      $data = DB::connection('mysql')->select($query);

      $query2 = "SELECT * FROM forms a INNER JOIN users b ON a.agent_id = b.id WHERE a.form_num = '$id';";
      $data2 = DB::connection('mysql')->select($query2);

      $result = array($data, $data2);

      return json_encode($result);
    }

    public function searchFormID2()
    {
      $id = Input::get("id");
      $form = Form::where('form_num', $id )->get();
      return json_encode($form);
    }



    public function updateFormStatus()
    {
      $verifier = Input::get('update_from');
      $form_number  = Input::get("form_num");
      if($verifier == "verifier")
      {
        $update = Form::where('form_num', '=', $form_number)->update([
          'input_date' => Input::get('input_date2'),
          'plan_holder' => Input::get('plan_holder2'),
          'birth_date' => Input::get('birth_date2'),
          'address' => Input::get('address2'),
          'postcode' => Input::get('postcode2'),
          'telephone' => Input::get('telephone2'),
          'plan_applicant' => Input::get('plan_applicant2'),
          'applicant_relationship' => Input::get('applicant_relationship2'),
          'app_rel_address' => Input::get('app_rel_address2'),
          'app_rel_postcode' => Input::get('app_rel_postcode2'),
          'app_rel_tel' => Input::get('app_rel_tel2'),
          'app_rel_email' => Input::get('app_rel_email2'),
          'is_notified_arrangement' => Input::get('is_notified_arrangement2'),
          'executors' => Input::get('executors2'),
          'executors_tel' => Input::get('executors_tel2'),
          'funeral_type' => Input::get('funeral_type2'),
          'funeral_plan_type' => Input::get('funeral_plan_type2'),
          'funeral_plan_type' => Input::get('funeral_plan_type2'),
          'payment_option' => Input::get('payment_option2'),
          'payment_full_type_selected' => Input::get('payment_full_type_selected2'),
          'payment_full_amount_selected' => Input::get('payment_full_amount_selected2'),
          'payment_installment_type_selected' => Input::get('payment_installment_type_selected2'),
          'payment_installment_amount_selected' => Input::get('payment_installment_amount_selected2'),
          'payment_fixed_age_selected' => Input::get('payment_fixed_age_selected2'),
          'payment_fixed_type_selected' => Input::get('payment_fixed_type_selected2'),
          'payment_fixed_monthly_selected' => Input::get('payment_fixed_monthly_selected2'),
          'status' => Input::get('status2'),
          'comment' => Input::get('comment2'),
          'action' => Input::get('action2'),
          'success_transfer' => Input::get('success_transfer2'),
          'unsuccess_transfer' => Input::get('unsuccess_transfer2'),
          'hub_comment' => Input::get('hub_comment2'),
          'updated_action' => Input::get('updated_action2'),
          'planholder_email' => Input::get('planholder_email2')
          ]);

          if($update == 1)
          {
            Session::flash('alert-success', 'Status tagging success!');
          }
          else
          {
            Session::flash('alert-danger', 'Status tagging failed! Please try again.');
          }

          return Redirect::to('form-update');
      }
      else
      {
        $rules = array(
            'form_num'             => 'required',
            'status'            => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('view-form')->withErrors($validator);
        }
        else
        {
          $form_number  = Input::get("form_num");
          $status       = Input::get("status");
          $agent_special_note     = Input::get("agent_special_note");

          $update = Form::where('form_num', '=', $form_number)->update(['status' => $status, 'agent_special_note' => $agent_special_note]);

          if($update == 1)
          {
            Session::flash('alert-success', 'Status tagging success!');
          }
          else
          {
            Session::flash('alert-danger', 'Status tagging failed! Please try again.');
          }

          return Redirect::to('view-form');
        }
      }


    }

    public function getFormList()
    {
      $from          = Input::get('from');
      $to            = Input::get('to');
      $action        = Input::get('action');
      $agent         = Input::get('agent');
      $action_query  = "";
      $agent_query   = "";

      if($action != "" && $action != "All")
      {
        $action_query = "AND a.action ='$action'";
      }

      if($agent != "")
      {
        $agent_query = "AND a.agent_id ='$agent'";
      }

      $query = "SELECT a.id, a.form_num, a.input_date, a.plan_holder, a.telephone, a.status, b.name, a.agent_special_note, a.comment as qdf_comment, a.action as agent_action, a.hub_comment as hub_comment, a.updated_action  as verifier_action, a.updated_at
      FROM forms a INNER JOIN users b ON a.agent_id = b.id WHERE a.input_date >= '$from' AND a.input_date <= '$to' $action_query $agent_query ORDER BY a.id ASC;";

      $data = DB::connection('mysql')->select($query);
      return json_encode($data);

    }

    public function getFormOnLoad()
    {
      $data = Form::join('users', 'forms.agent_id', '=', 'users.id')
      ->select(['forms.id', 'forms.form_num', 'forms.input_date', 'forms.plan_holder', 'forms.telephone', 'users.name', 'forms.comment', 'forms.agent_special_note', 'forms.action', 'forms.status', 'forms.updated_at']);

      return Datatables::of($data)->make();

    }

    public function showUpdateDetails()
    {

      if(Auth::check())
      {
        if(Auth::user()->access_level == 3)
        {
          return view('form.update-form');
        }
        else
        {
            return Response::view('errors.404', array(), 404);
        }
      }
      else
      {
        return Redirect::to('auth/login');
      }
    }

    public function payment()
    {
      return view('form.payment');
    }
}

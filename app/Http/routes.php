<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
  if(Auth::check())
  {
    return view('/home');
  }
  else
  {
    return view('auth.login');
  }

});

Route::get('/home', function () {
    return view('home');
});

Route::resource('form', 'FormController');
Route::get('view-form', 'FormController@showViewForm');

Route::get('search-id', 'FormController@searchFormID');
Route::get('search-id2', 'FormController@searchFormID2');
Route::get('form-list', 'FormController@getFormList');
Route::get('form-list-onload', 'FormController@getFormOnLoad');
Route::get('get-autonumber', 'FormController@getNumberForm');
Route::post('update-form', 'FormController@updateFormStatus');
Route::get('form-update', 'FormController@showUpdateDetails');
Route::get('packout-email', 'PackoutController@showPackoutEmail');
Route::post('packout-email', 'PackoutController@processPackoutEmail');
Route::get('webmail-sent', 'PackoutController@showWebmailSent');
Route::get('ajax-webmail-sent', 'PackoutController@ajaxWebmailSent');


// Payment Routes
Route::get('test-payment', 'FormController@payment');

// Report Controller
Route::get('reports/eosr', 'ReportController@showEosrReport');
Route::get('reports/eosr-get', 'ReportController@getEosrReport');
Route::get('reports/report1', 'ReportController@showReport1');
Route::get('reports/report1-get', 'ReportController@getReport1');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

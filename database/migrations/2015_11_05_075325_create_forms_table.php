<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('forms', function (Blueprint $table) {
          $table->increments('id');
          $table->date('input_date');
          $table->string('plan_holder');
          $table->date('birth_date');
          $table->string('address');
          $table->string('postcode');
          $table->string('telephone');
          $table->string('plan_applicant');
          $table->string('applicant_relationship');
          $table->string('app_rel_address');
          $table->string('app_rel_postcode');
          $table->string('app_rel_tel');
          $table->string('app_rel_email');
          $table->string('is_notified_arrangement');
          $table->string('executors');
          $table->string('executors_tel');
          $table->string('executors_address');
          $table->string('funeral_type');
          $table->string('funeral_plan_type');
          $table->string('funeral_details_notes');
          $table->string('payment_option');
          $table->string('payment_full_type_selected');
          $table->string('payment_full_amount_selected');
          $table->string('payment_installment_type_selected');
          $table->string('payment_installment_amount_selected');
          $table->string('payment_fixed_age_selected');
          $table->string('payment_fixed_type_selected');
          $table->string('payment_fixed_monthly_selected');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

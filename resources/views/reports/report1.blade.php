@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Forms Spreadsheet Report</div>
				<div class="panel-body">
					<div class="form-horizontal">
						
						<div class="form-group">
							<label class="col-md-4 control-label">From</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="datePicker" id="datePickerFrom" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">To</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="datePicker" id="datePickerTo" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Agents</label>
							<div class="col-md-6">
								{!! Form::select('agent_name', $agent_options, '',array('class' => 'form-control', 'id' => 'agent_name')) !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" id="SprBtnGenerate" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection
@section('spr')


<script type="text/javascript">
	jQuery('#datePickerFrom, #datePickerTo').datetimepicker({
	  format:'Y-m-d H:i:s'
	});
	$("#SprBtnGenerate").click(function() {
		var page = 'report1-get?from='+$("#datePickerFrom").val()+'&to='+$("#datePickerTo").val()+'&agent_id='+$("#agent_name").val();
		confirm("I will now process your query. Please wait while processing. Do you want to continue?");
		return window.open(page);
	});
</script>

@endsection

@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">End of Shift Report</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-md-4 control-label">From</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="datePicker" id="datePickerFrom" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">To</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="datePicker" id="datePickerTo" required>
							</div>
						</div>
						<!-- <div class="form-group">
							<label class="col-md-4 control-label">Application Status</label>
							<div class="col-md-6">
								<select class="form-control" name="application_status" id="application_status">
									<option value=""></option>
									<option value="Pending">Pending</option>
									<option value="Hired">Hired</option>
									<option value="Retracted">Retracted</option>
									<option value="No Show">No Show</option>
									<option value="Failed - Ineligible for Re-hire">Failed - Ineligible for Re-hire</option>
									<option value="Failed – Reapply in 30 -60 days">Failed – Reapply in 30 -60 days</option>
								</select>
							</div>
						</div> -->
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" id="EosrBtnGenerate" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">End of Shift Report</div>
				<div class="panel-body">
                    <table id="EosrTbl" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Agent Name</th>
                                <th>Grand Total</th>
                                <th>Packout Mail</th>
                                <th>Packout E-Mail</th>
                                <th>Callback</th>
                                <th>Sale</th>
                                <th>Rejected</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
													<tr>
															<th>Grand Total</th>
															<th id="gtGrandTotal"></th>
															<th id="packoutMailGt"></th>
															<th id="packoutEMailGt"></th>
															<th id="callbackGt"></th>
															<th id="saleGt"></th>
															<th id="rekectedGt"></th>
													</tr>
												</tfoot>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('eosr')
<script type="text/javascript">
jQuery('#datePickerFrom, #datePickerTo').datetimepicker({
  format:'Y-m-d H:i:s'
});
$("#EosrBtnGenerate").click(function() {
	table = $('#EosrTbl').DataTable();
	$.ajax({
		url: "eosr-get",
		type: 'GET',
		data: {"from" : $("#datePickerFrom").val(), "to" :  $("#datePickerTo").val()},
		success: function(result){
		var myObj = $.parseJSON(result);
		var gtGrandTotal = 0;
		var packoutMailGt = 0;
		var packoutEMailGt = 0;
		var callbackGt = 0;
		var saleGt = 0;
		var rekectedGt = 0;

	    	$.each(myObj, function(key,value) {
					var grandtotal = parseInt(value.packoutmail) + parseInt(value.packoutemail) +  parseInt(value.callback) + parseInt(value.rejected) + parseInt(value.sale);
					gtGrandTotal += grandtotal;
					packoutMailGt += parseInt(value.packoutmail);
					packoutEMailGt += parseInt(value.packoutemail);
					callbackGt += parseInt(value.callback);
					saleGt += parseInt(value.sale);
					rekectedGt += parseInt(value.rejected);

	    		table.row.add( [
						value.name,
						grandtotal,
						value.packoutmail,
						value.packoutemail,
						value.callback,
					//	value.pending,
						value.sale,
						value.rejected
	        	] ).draw();
			});
			$("#gtGrandTotal").text(gtGrandTotal);
			$("#packoutMailGt").text(packoutMailGt);
			$("#packoutEMailGt").text(packoutEMailGt);
			$("#callbackGt").text(callbackGt);
			$("#saleGt").text(saleGt);
			$("#rekectedGt").text(rekectedGt);


		}});
});



	</script>
@endsection

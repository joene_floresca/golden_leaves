<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Golden Leaves</title>

	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery.datetimepicker.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<!-- <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ asset('bootflat/css/bootflat.css') }}" rel="stylesheet">
	<link href="{{ asset('bootflat/css/bootflat.css.map') }}" rel="stylesheet"> -->

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
	<link href="{{ asset('/css/dataTables.tableTools.css') }}" rel="stylesheet">


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('home') }}">Golden Leaves</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<!-- <li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ url('deliveries') }}">Deliveries</a></li>
					<li><a href="{{ url('inhouse') }}">Inhouse Campaigns</a></li> -->
					<!-- <li><a href="{{ url('deliverytracker') }}">Delivery Tracking</a></li> -->
					<li><a href="{{ url('home') }}"><span class="glyphicon glyphicon-home"></span></a></li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Create Form"><span class="glyphicon glyphicon-th-list"></span>&nbsp; View Plans <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('home#silver') }}">Silver Plan</a></li>
							<li><a href="{{ url('home#gold') }}">Gold Plan</a></li>
							<li><a href="{{ url('home#platinum') }}">Platinum Plan</a></li>
							<li><a href="{{ url('home#comparison') }}">Plan Comparison</a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Create Form"><span class="glyphicon glyphicon-gbp"></span>&nbsp; View Payment Scheme <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('home#installment') }}">The 12-60 month option</a></li>
							<li><a href="{{ url('home#fixedmonthly') }}">Fixed Monthly Payment scheme</a></li>
						</ul>
					</li>

						@if (Auth::check() && Auth::user()->access_level == 0)
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Create Form"><span class="glyphicon glyphicon-pencil"></span>&nbsp; Forms <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('form/create') }}">Create Form</a></li>
								<li><a href="{{ url('form') }}">View Forms</a></li>
								<li><a href="{{ url('packout-email') }}">Packout</a></li>
								<li><a href="{{ url('view-form') }}">Verify Forms</a></li>
							</ul>
						</li>
						@elseif(Auth::check() && Auth::user()->access_level == 1)
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="View Form"><span class="glyphicon glyphicon-search"></span>&nbsp; Forms <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('view-form') }}">View Form</a></li>
						</ul>
					</li>
					@elseif(Auth::check() && Auth::user()->access_level == 3)
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="View Form"><span class="glyphicon glyphicon glyphicon-list-alt"></span>&nbsp; Forms <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('form') }}">View Forms</a></li>
							<li><a href="{{ url('view-form') }}">Verify Forms</a></li>
							<li><a href="{{ url('form-update') }}">Update Details</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="View Form"><span class="glyphicon glyphicon-tasks"></span>&nbsp; Agent <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('form/create') }}">New Prospect</a></li>
							<li><a href="{{ url('packout-email') }}">Packout</a></li>
							<li><a href="http://83.223.123.56:2095/cpsess81707558/3rdparty/squirrelmail/src/webmail.php" target="_blank">Webmail Inbox</a></li>
							<li><a href="{{ url('webmail-sent') }}">Webmail Sent</a></li>
						</ul>
					</li>
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="View Form"><span class="glyphicon glyphicon-folder-open"></span>&nbsp; Reports <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('reports/eosr') }}">End of Shift Report</a></li>
							<li><a href="{{ url('reports/report1') }}">Spreadsheet Report</a></li>
						</ul>
					</li>
					@endif

				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp; &nbsp;{{ Auth::user()->name }}<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')



	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script src="{{ asset('js/dataTables.tableTools.js') }}"></script>
	<script src="{{ asset('js/jquery.datetimepicker.js') }}"></script>
	<script src="{{ asset('js/script.js') }}"></script>
	<script src="{{ asset('js/range_dates.js') }}"></script>
	<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
	@yield('form-create')
	@yield('form-view')
	@yield('form-index')
	@yield('form-update')
	@yield('eosr')
	@yield('spr')
	@yield('packout-email')
	@yield('packout-email-sent')

</body>
</html>

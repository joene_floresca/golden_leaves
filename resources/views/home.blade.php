@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default" id="silver">
				<div class="panel-heading" id="silverH1">Silver Plan</div>
				<div class="panel-body">
						<ul>
							<li>The <strong class="text-danger">Silver Plan</strong> starts from just £26.40 per month and covers the Funeral Director’s fees, as well as a hearse to meet with mourners at the crematorium or cemetery.</li>
								  <ol>
										<li>Administration of plan</li>
										<li>The funeral directors professional services</li>
										<li>Advice on registration, documentation and certification for the funeral</li>
										<li>Removal from place of death to funeral directors premises within 25 miles (9am-5pm)</li>
										<li>Care of deceased prior to funeral</li>
										<li>A basic coffin</li>
										<li>Attendance of conductor and four bearers on day of funeral</li>
										<li>Provision of hearse to crematorium chapel only</li>
										<li>Bereavement counselling (where available) </li>
										<li>Prices from </li>
											<ul>
												<li>Funeral Director Services Costs (£2135.00)</li>
												<li>Disbursement Costs</li>
												<li>Plan Administration Fee (£185.00)</li>
												<li>Disbursements (if included)*</li>
													<ol>
														<li>Ministers fees </li>
														<li> Doctors’ fees </li>
														<li> Crematorium fees </li>
													</ol>
											</ul>
									</ol>
						</ul>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" id="gold">Gold Plan</div>
				<div class="panel-body">
					<ul>
						<li>The <strong class="text-danger">Gold Plan</strong> starts from just £29.51 per month and covers the Funeral Director’s fees, a hearse and one limousine for mourners.
							</li>

								<ol>
									<li>Administration of plan</li>
									<li>The funeral directors professional services</li>
									<li>Advice on registration, documentation and certification for the funeral</li>
									<li>Removal from place of death to funeral directors premises within 25 miles (24h)</li>
									<li>Removal from place of death to funeral directors premises within 25 miles (24h)</li>
									<li>Care of deceased prior to funeral</li>
									<li>Use of chapel of rest for visits by family and friends</li>
									<li>A standard coffin</li>
									<li>Attendance of conductor and four bearers on day of funeral</li>
									<li>Provision of hearse and one limousine for service at Crematorium/Cemetery</li>
									<li>Full listing of floral tributes</li>
									<li>Bereavement counselling (where available)</li>
									<li>Prices from :</li>
										<ul>
											<li>Funeral Director Services Costs  (£2515.00) </li>
											<li> Disbursement Costs </li>
											<li> Plan Administration Fee (£185.00) </li>
											<li> Disbursements (if included)* </li>
												<ol>
													<li>Ministers fees</li>
													<li>Doctors’ fee</li>
													<li>Crematorium fees</li>
												</ol>
										</ul>
								</ol>
					</ul>
				</div>
			</div>

			<div class="panel panel-default" id="platinum">
				<div class="panel-heading" id="platinumH1">Platinum Plan</div>
				<div class="panel-body">
					<ul>
						<li>The <strong class="text-danger">Platinum Plan</strong> starts from just £32.14 per month and covers the Funeral Director’s fees, a hearse and two limousines for mourners.
						</li>

						 <ol>
							 <li>Administration of plan</li>
							 <li>The funeral directors professional services</li>
							 <li>Advice on registration, documentation and certification for the funeral</li>
							 <li>Removal from place of death to funeral directors premises within 25 miles (24h)</li>
							 <li>Care of deceased prior to funeral</li>
							 <li>Use of chapel of rest for visits by family and friends</li>
							 <li>A high quality coffin</li>
							 <li>Attendance of conductor and four bearers on day of funeral</li>
							 <li>Provision of hearse and two limousines for service at Crematorium/Cemetery</li>
							 <li>Full listing of floral tributes</li>
							 <li>Thank you cards</li>
							 <li>Bereavement counselling (where available) </li>
							 <li>Prices from: </li>
								<ul>
									<li>Funeral Director Services Costs (£2835.00)</li>
									<li>Disbursement Costs</li>
									<li>Plan Administration Fee (£185.00)</li>
									<li>Disbursements (if included)*</li>
										<ol>
											 <li>Ministers fees</li>
											 <li>Doctors’ fees</li>
											 <li>Crematorium fees</li>
										</ol>
								</ul>
						 </ol>
					</ul>
				</div>
			</div>

			<div class="panel panel-success">
				<div class="panel-heading">Payment Schemes</div>
				<div class="panel-body">
					<div class="col-md-12" id="installment">
						<h4>A grid showing monthly payment details for the 12-60 month option:</h4>
						<br/>
						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label"><img class="img" src="{{ asset('images/silver.png') }}"></label>
							<div class="col-md-8">
								<!-- <img src="{{ asset('images/silver.JPG') }}"> -->

	<table class="table table-bordered table-striped">
    <thead class="blackThead">
      <tr>
        <th>Cost of the Simple Plan</th>
        <th>£3420.00</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>12 Monthly Installments of</td>
        <td>£285.00</td>
      </tr>
      <tr>
        <td>Total after 12 Months</td>
        <td>£3420.00</td>
      </tr>
      <tr>
        <td>36 Monthly Installments of</td>
        <td>£106.74</td>
      </tr>
      <tr>
        <td>Total after 36 Months</td>
        <td>£3842.71</td>
      </tr>
      <tr>
        <td>60 Monthly Installments of</td>
        <td>£71.96</td>
      </tr>
      <tr>
        <td>Total after 60 Months</td>
        <td>£4317.67</td>
      </tr>
    </tbody>
  </table>
							</div>


						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label"><img class="img" src="{{ asset('images/gold.png') }}"></label>
							<div class="col-md-8">
								<!-- <img src="{{ asset('images/gold.JPG') }}"> -->

	<table class="table table-bordered table-striped">
    <thead class="blackThead">
      <tr>
        <th>Cost of the Gold Plan</th>
        <th>£3800.00</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>12 Monthly Installments of</td>
        <td>£316.67</td>
      </tr>
      <tr>
        <td>Total after 12 Months</td>
        <td>£3800.00</td>
      </tr>
      <tr>
        <td>36 Monthly Installments of</td>
        <td>£118.60</td>
      </tr>
      <tr>
        <td>Total after 36 Months</td>
        <td>£4269.68</td>
      </tr>
      <tr>
        <td>60 Monthly Installments of</td>
        <td>£79.96</td>
      </tr>
      <tr>
        <td>Total after 60 Months</td>
        <td>£4797.41</td>
      </tr>
    </tbody>
  </table>

							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label"><img class="img" src="{{ asset('images/platinum.png') }}"></label>
							<div class="col-md-8">
								<!-- <img src="{{ asset('images/platinum.JPG') }}"> -->

								<table class="table table-bordered table-striped">
    <thead class="blackThead">
      <tr>
        <th>Cost of the Platinum Plan</th>
        <th>£4120.00</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>12 Monthly Installments of</td>
        <td>£343.33</td>
      </tr>
      <tr>
        <td>Total after 12 Months</td>
        <td>£4120.00</td>
      </tr>
      <tr>
        <td>36 Monthly Installments of</td>
        <td>£128.59</td>
      </tr>
      <tr>
        <td>Total after 36 Months</td>
        <td>£4629.23</td>
      </tr>
      <tr>
        <td>60 Monthly Installments of</td>
        <td>£86.69</td>
      </tr>
      <tr>
        <td>Total after 60 Months</td>
        <td>£5201.41</td>
      </tr>
    </tbody>
  </table>
							</div>
						</div>
					</div>

				<div class="col-md-12" id="fixedmonthly">
						<br/>
						<h4>A grid showing the Fixed Monthly Payment scheme: </h4>
						<!--	<img src="{{ asset('images/ref.JPG') }}">-->
						 


   <table class="table orange">
    <thead id="orange">
      <tr>
        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th>FROM50 Bronze&nbsp;&nbsp;&nbsp;</th>
        <th>FROM50 Silver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th>FROM50 Gold&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th>FROM50 Platinum</th>
      </tr>
    </thead>
    
  </tbody>
</table>
	<table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>Your Age</th>
        <th>Monthly Payment</th>
        <th>Monthly Payment</th>
        <th>Monthly Payment</th>
        <th>Monthly Payment</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>50</td>
        <td>£19.02</td>
        <td>£27.44</td>
        <td>£30.49</td>
        <td>£33.06</td>       
      </tr>
      <tr>
        <td>51</td>
        <td>£19.18</td>
        <td>£27.68</td>
        <td>£30.76</td>
        <td>£33.35</td>       
      </tr>
      <tr>
        <td>52</td>
        <td>£19.37</td>
        <td>£27.95</td>
        <td>£31.06</td>
        <td>£33.67</td>       
      </tr>
      <tr>
        <td>53</td>
        <td>£19.58</td>
        <td>£28.26</td>
        <td>£31.40</td>
        <td>£34.05</td>       
      </tr>
      <tr>
        <td>54</td>
        <td>£19.83</td>
        <td>£28.62</td>
        <td>£31.80</td>
        <td>£34.48</td>       
      </tr>
      <tr>
        <td>55</td>
        <td>£20.10</td>
        <td>£29.00</td>
        <td>£32.23</td>
        <td>£34.94</td>       
      </tr>
      <tr>
        <td>56</td>
        <td>£20.39</td>
        <td>£29.42</td>
        <td>£32.68</td>
        <td>£35.44</td>       
      </tr>
      <tr>
        <td>57</td>
        <td>£20.69</td>
        <td>£29.86</td>
        <td>£33.17</td>
        <td>£35.97</td>       
      </tr>
      <tr>
        <td>58</td>
        <td>£21.02</td>
        <td>£30.33</td>
        <td>£33.70</td>
        <td>£36.53</td>       
      </tr>
      <tr>
        <td>59</td>
        <td>£21.36</td>
        <td>£30.83</td>
        <td>£34.25</td>
        <td>£37.14</td>       
      </tr>
      <tr>
        <td>60</td>
        <td>£21.83</td>
        <td>£31.50</td>
        <td>£35.00</td>
        <td>£37.94</td>       
      </tr>
      <tr>
        <td>61</td>
        <td>£22.26</td>
        <td>£32.12</td>
        <td>£35.69</td>
        <td>£38.69</td>       
      </tr>
      <tr>
        <td>62</td>
        <td>£22.71</td>
        <td>£32.77</td>
        <td>£36.41</td>
        <td>£39.47</td>       
      </tr>
      <tr>
        <td>63</td>
        <td>£23.20</td>
        <td>£33.48</td>
        <td>£37.20</td>
        <td>£40.33</td>       
      </tr>
      <tr>
        <td>64</td>
        <td>£23.74</td>
        <td>£34.26</td>
        <td>£38.06</td>
        <td>£41.27</td>       
      </tr>
      <tr>
        <td>65</td>
        <td>£24.34</td>
        <td>£35.12</td>
        <td>£39.02</td>
        <td>£42.31</td>       
      </tr>
      <tr>
        <td>66</td>
        <td>£24.87</td>
        <td>£35.88</td>
        <td>£39.87</td>
        <td>£43.23</td>       
      </tr>
      <tr>
        <td>67</td>
        <td>£25.59</td>
        <td>£36.93</td>
        <td>£41.03</td>
        <td>£44.49</td>       
      </tr>
      <tr>
        <td>68</td>
        <td>£26.39</td>
        <td>£38.09</td>
        <td>£42.32</td>
        <td>£45.88</td>       
      </tr>   
      <tr>
        <td>69</td>
        <td>£27.29</td>
        <td>£39.38</td>
        <td>£43.75</td>
        <td>£47.44</td>       
      </tr>
      <tr>
        <td>70</td>
        <td>£28.27</td>
        <td>£40.79</td>
        <td>£45.33</td>
        <td>£49.14</td>       
      </tr>
      <tr>
        <td>71</td>
        <td>£29.35</td>
        <td>£42.36</td>
        <td>£47.06</td>
        <td>£51.02</td>       
      </tr>
      <tr>
        <td>72</td>
        <td>£30.54</td>
        <td>£44.08</td>
        <td>£48.97</td>
        <td>£53.10</td>       
      </tr>
      <tr>
        <td>73</td>
        <td>£31.87</td>
        <td>£45.99</td>
        <td>£51.10</td>
        <td>£55.40</td>       
      </tr>
      <tr>
        <td>74</td>
        <td>£33.34</td>
        <td>£48.12</td>
        <td>£53.46</td>
        <td>£57.97</td>       
      </tr>
      <tr>
        <td>75</td>
        <td>£35.06</td>
        <td>£50.60</td>
        <td>£56.22</td>
        <td>£60.96</td>       
      </tr>
</div>
      
      
    </tbody>
  </table>

				</div>
			</div>
</div>
			<div class="panel panel-success" id="comparison">
				<div class="panel-heading">Plan Comparison</div>
				<div class="panel-body">
					
<table class="table table-hover table-bordered">
    <thead class="blackThead">
      <tr>
        <th>Choose from the following Plans</th>
        <th>Simple</th>
        <th>Standard</th>
        <th>Platinum</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Funeral Director's professional services</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Advice on funeral registration, documentation and certification </td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Removal from place of death to Funeral Director's premises within 25 miles in normal working hours</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td>24 Hours</td>
        <td>24 Hours</td>
      </tr>
      <tr>
        <td>Care of deceased prior to funeral</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Chapel of Rest available for family and friends to visit</td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Basic coffin </td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td></td>
        <td></td>
      </tr>

      <tr>
        <td>Standard coffin</td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td></td>
      </tr>

      <tr>
        <td>High quality coffin </td>
        <td></td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Attendance of conductor and four pallbearers on day of funeral</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Provision of hearse for service at Crematorium / Cemetery</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Provision of one limousine</td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td></td>
      </tr>
      <tr>
        <td>Provision of two limousine</td>
        <td></td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
       <tr>
        <td>Full listing of floral tributes</td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
       <tr>
        <td>Thank you cards</td>
        <td></td>
        <td></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Bereavement counselling (where available)</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
      <tr>
        <td>Disbursements</td>
        <td><span class="glyphicon glyphicon glyphicon-ok"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok orange"></span></td>
        <td><span class="glyphicon glyphicon glyphicon-ok blue"></span></td>
      </tr>
    </tbody>
  </table>


				</div>
			</div>


		</div>
	</div>
</div>
@endsection

@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Application Form</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
			        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
			          @if(Session::has('alert-' . $msg))
								<div  class="alert alert-{{ $msg }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			          <p>{{ Session::get('alert-' . $msg) }}</p>
							</div>
			          @endif
			        @endforeach
			    </div>

					<div class="form-horizontal" role="form">


        <div class="col-lg-12" style="padding-top: 10px;">
            <!-- <table class="table table-bordered">
              <tr>
                  <td>Date</td>
                  <td id="input_date"></td>
              <tr/>
              <tr>
                  <td>Plan Holder</td>
                  <td id="plan_holder"></td>
              <tr/>
              <tr>
                  <td>Date of Birth</td>
                  <td id="birth_date"></td>
              <tr/>
              <tr>
                  <td>Address</td>
                  <td id="address"></td>
              <tr/>
              <tr>
                  <td>Postcode</td>
                  <td id="postcode"></td>
              <tr/>
              <tr>
                  <td>Telephone</td>
                  <td id="telephone"></td>
              <tr/>
              <tr>
                  <td>Plan Applicant</td>
                  <td id="plan_applicant"></td>
              <tr/>
              <tr>
                  <td>Relationship</td>
                  <td id="applicant_relationship"></td>
              <tr/>
              <tr>
                  <td>Address</td>
                  <td id="app_rel_address"></td>
              <tr/>
              <tr>
                  <td>Postcode</td>
                  <td id="app_rel_postcode"></td>
              <tr/>
              <tr>
                  <td>Telephone</td>
                  <td id="app_rel_tel"></td>
              <tr/>
              <tr>
                  <td>Email</td>
                  <td id="app_rel_email"></td>
              <tr/>
              <tr>
                  <td>Do you wish them to be notified of the funeral plan and the arrangements you have made?</td>
                  <td id="is_notified_arrangement"></td>
              <tr/>
              <tr>
                  <td>My executors are</td>
                  <td id="executors"></td>
              <tr/>
              <tr>
                  <td>Telephone</td>
                  <td id="executors_tel"></td>
              <tr/>
              <tr>
                  <td>Address</td>
                  <td id="executors_address"></td>
              <tr/>

              <tr>
                  <td>Funeral Type</td>
                  <td id="funeral_type"></td>
              <tr/>
              <tr>
                  <td>Plan Type</td>
                  <td id="funeral_plan_type"></td>
              <tr/>
              <tr>
                  <td>Special Requests / Notes</td>
                  <td id="funeral_details_notes"></td>
              <tr/>
              <tr>
                  <td>Payment Option</td>
                  <td id="payment_option"></td>
              <tr/>
              <tr>
                  <td>Full Payment Type</td>
                  <td id="payment_full_type_selected"></td>
              <tr/>
              <tr>
                  <td>Full Payment Amount</td>
                  <td id="payment_full_amount_selected"></td>
              <tr/>
              <tr>
                  <td>Pay in 12-60 monthly instalments Type</td>
                  <td id="payment_installment_type_selected"></td>
              <tr/>
							<tr>
                  <td>Pay in 12-60 monthly instalments Amount</td>
                  <td id="payment_installment_amount_selected"></td>
              <tr/>
              <tr>
                  <td>Pay by fixed monthly Age</td>
                  <td id="payment_fixed_age_selected"></td>
              <tr/>
							<tr>
                  <td>Pay by fixed monthly Type</td>
                  <td id="payment_fixed_type_selected"></td>
              <tr/>
							<tr>
                  <td>Pay by fixed monthly Amount</td>
                  <td id="payment_fixed_monthly_selected"></td>
              <tr/>
            </table> -->
        </div>

				<form class="form-horizontal" role="form" method="POST" action="{{ url('update-form') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="col-lg-12">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default" id="searchBtn" type="button">Go!</button>
							</span>
							<input type="text" class="form-control" id="searchID" name="form_num" placeholder="Search for form number..">
						</div>
					</div>
				<br/><br/>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Date</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="input_date"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Plan Holder</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="plan_holder"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Date of Birth</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="birth_date"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Address</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="address"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Postcode</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="postcode"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Telephone</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="telephone"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Plan Applicant</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="plan_applicant"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Relationship</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="applicant_relationship"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Address</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="app_rel_address"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Postcode</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="app_rel_postcode"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Telephone</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="app_rel_tel"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Email</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="app_rel_email"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Do you wish them to be notified of the funeral plan and the arrangements you have made?</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="is_notified_arrangement"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">My executors are</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="executors"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Telephone</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="executors_tel"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Funeral Type</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="funeral_type"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Plan Type</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="funeral_plan_type"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Special Requests / Notes</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="funeral_details_notes"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Payment Option</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_option"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Full Payment Type</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_full_type_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Full Payment Amount</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_full_amount_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Pay in 12-60 monthly instalments Type</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_installment_type_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Pay in 12-60 monthly instalments Amount</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_installment_amount_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Pay by fixed monthly Age</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_fixed_age_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Pay by fixed monthly Type</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_fixed_type_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Pay by fixed monthly Amount</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="payment_fixed_monthly_selected"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Assigned Status</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="status"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Comments</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="comments"></label>
							<label class="col-md-4 control-label" id="comment" name="comment"></label>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Agent Special Request/Notes</label>
						<div class="col-md-6">
							<label class="col-md-4 control-label" id="comments"></label>
							<textarea class="form-control" rows="5" name="agent_special_note" id="agent_special_note"></textarea>
						</div>
					</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Final Agent Action</label>
						<div class="col-md-6">
							<select class="form-control" name="status" id="status">
								<option value=""></option>
								<option value="Packout B">Packout B</option>
								<option value="Callback">Callback</option>
								<option value="Reject">Reject</option>
								<option value="Sale">Sale</option>
								<option value="Packout Mail">Packout Mail</option>
							</select>
						</div>
					</div>

					<input type="hidden" class="form-control" name="update_from" id="update_from" value="" />

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
						</div>
					</div>

				</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('form-view')
<script>
$("#searchBtn").click(function() {
  var id = $("#searchID").val();
  $.ajax({
        url: "search-id2",
        type: 'GET',
        data: {"id" : id},
        success: function(result){
        var myObj = $.parseJSON(result);
				console.log(myObj);
        $('#input_date').append(myObj[0].input_date);
        $('#plan_holder').append(myObj[0].plan_holder);
        $('#birth_date').append(myObj[0].birth_date);
        $('#address').append(myObj[0].address);
        $('#postcode').append(myObj[0].postcode);
        $('#telephone').append(myObj[0].telephone);
        $('#plan_applicant').append(myObj[0].plan_applicant);
        $('#applicant_relationship').append(myObj[0].applicant_relationship);
        $('#app_rel_address').append(myObj[0].app_rel_address);
        $('#app_rel_postcode').append(myObj[0].app_rel_postcode);
        $('#app_rel_tel').append(myObj[0].app_rel_tel);
        $('#app_rel_email').append(myObj[0].app_rel_email);
        $('#is_notified_arrangement').append(myObj[0].is_notified_arrangement);
        $('#executors').append(myObj[0].executors);
        $('#executors_tel').append(myObj[0].executors_tel);
        $('#executors_address').append(myObj[0].executors_address);
        $('#funeral_type').append(myObj[0].funeral_type);
        $('#funeral_plan_type').append(myObj[0].funeral_plan_type);
        $('#funeral_details_notes').append(myObj[0].funeral_details_notes);
        $('#payment_option').append(myObj[0].payment_option);
        $('#payment_full_type_selected').append(myObj[0].payment_full_type_selected);
        $('#payment_full_amount_selected').append(myObj[0].payment_full_amount_selected);
        $('#payment_installment_type_selected').append(myObj[0].payment_installment_type_selected);
        $('#payment_installment_amount_selected').append(myObj[0].payment_installment_amount_selected);
        $('#payment_fixed_age_selected').append(myObj[0].payment_fixed_age_selected);
        $('#payment_fixed_type_selected').append(myObj[0].payment_fixed_type_selected);
        $('#payment_fixed_monthly_selected').append(myObj[0].payment_fixed_monthly_selected);
		$('#status').val(myObj[0].status).prop('selected', true);
        $('#comment').append(myObj[0].comment);
        $('#agent_special_note').append(myObj[0].agent_special_note);

    }});
});

</script>
@endsection

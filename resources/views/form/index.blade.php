
@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">


<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form List</div>
				<div class="panel-body">
					<div class="form-horizontal">

						<div class="form-group">
							<label class="col-md-4 control-label">From</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="datePicker" id="datePickerFrom" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">To</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="datePicker" id="datePickerTo" required>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Actions</label>
							<div class="col-md-6">
								<select class="form-control" name="action" id="action">
									<option value="">Choose One</option>
									<option value="Unsuccessful Transfer">Unsuccessful Transfer</option>
									<option value="Successful Transfer">Successful Transfer</option>
									<option value="Pack-out">Pack-out</option>
									<option value="For callback">For callback</option>
									<option value="Successful Sale">Successful Sale</option>
									<option value="Pack-out Email A">Pack-out Email A</option>
									<option value="Pack-out Email B">Pack-out Email B</option>
									<option value="Pack-out mail">Pack-out mail</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Agents</label>
							<div class="col-md-6">
								{!! Form::select('agent_name', $agent_options, '',array('class' => 'form-control', 'id' => 'agent_name')) !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" id="FilterBtnGenerate" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>


		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Form List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
                    <table id="formList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            	<th colspan="11"> <center>Form Information</center></th>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Form #</th>
                                <th>Date Created</th>
                                <th>Plan Holder</th>
                                <th>Telephone</th>
                                <th>Agent Name</th>
                                <th>Verifier Comment</th>
                                <th>Agent Special Note/Request</th>
                                <th>Initial Agent Action</th>
                                <th>Final Agent Action</th>
                                <th>Last Updated</th>
                            </tr>
                        </thead>
                    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('form-index')

<script>

jQuery('#datePickerFrom, #datePickerTo').datetimepicker({
  format:'Y-m-d H:i:s'
});

var table = $('#formList').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'form-list-onload',
    "order": [[ 0, "desc" ]]
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var from = $("#datePickerFrom").val();
        var to = $("#datePickerTo").val();
        var date = data[3];
 
        if (date >=  from && date <= to)
        {
            return true;
        }
        return false;
    }
);


// var t =  $('#formList').DataTable({ "order": [[ 1, "desc" ]] });
// var tt = new $.fn.dataTable.TableTools(t);
// $( tt.fnContainer()).insertBefore('div.dataTables_wrapper');

$("#FilterBtnGenerate").click(function() {
	//var table = $('#formList').DataTable();
	 table.draw();
	// if($.fn.dataTable.isDataTable('#formList')) 
	// {
	// 	alert("its already a datatable");
	// 	alert(table);
	// }
	// if($.fn.dataTable.isDataTable('#formList')) 
	// {
	// 	alert("Hello Exist");
	// 	table.destroy();
 //    	table = $('#formList').DataTable({"order": [[ 0, "desc" ]]});
 //    	table.clear().draw();
 //    	var tt = new $.fn.dataTable.TableTools( table );
	//     $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');
	//     $.ajax({
	// 	url: "form-list",
	// 	type: 'GET',
	// 	data: {"from" : $("#datePickerFrom").val(), "to" :  $("#datePickerTo").val(), "action" : $("#action").val(), "agent" : $("#agent_name").val()},
	// 	success: function(result){
	// 	var myObj = $.parseJSON(result);
	//     	$.each(myObj, function(key,value) {
	//     		table.row.add( [
	// 	            value.id,
	// 	            value.form_num,
	// 	            value.input_date,
	// 	            value.plan_holder,
	// 	            value.telephone,
	// 	            value.name,
	// 	            value.qdf_comment,
	// 	            value.agent_special_note,
	// 	            value.agent_action,
	// 	            value.status,
	// 	            value.updated_at,
	//         	] ).draw();
	// 		});
	// 	}});
	// }
	// else
	// {
	// 	alert("Hello Not Exist");
	// 	table = $('#formList').DataTable({"order": [[ 0, "desc" ]]});
	// 	$.ajax({
	// 	url: "form-list",
	// 	type: 'GET',
	// 	data: {"from" : $("#datePickerFrom").val(), "to" :  $("#datePickerTo").val(), "action" : $("#action").val(), "agent" : $("#agent_name").val()},
	// 	success: function(result){
	// 	var myObj = $.parseJSON(result);
	//     	$.each(myObj, function(key,value) {
	//     		table.row.add( [
	// 	            value.id,
	// 	            value.form_num,
	// 	            value.input_date,
	// 	            value.plan_holder,
	// 	            value.telephone,
	// 	            value.name,
	// 	            value.qdf_comment,
	// 	            value.agent_special_note,
	// 	            value.agent_action,
	// 	            value.status,
	// 	            value.updated_at,
	//         	] ).draw();
	// 		});
	// 	}});

	// 	var tt = new $.fn.dataTable.TableTools( table );
	//     $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');
	// }
});

</script>

@endsection

@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Update Application Form</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
			        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
			          @if(Session::has('alert-' . $msg))
								<div  class="alert alert-{{ $msg }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			          <p>{{ Session::get('alert-' . $msg) }}</p>
							</div>
			          @endif
			        @endforeach
			    </div>

					<div class="form-horizontal" role="form">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('update-form') }}">
								<div class="col-lg-12" style="padding-bottom: 10px;">
									<div class="input-group">
										<span class="input-group-btn">
											<button class="btn btn-default" id="searchBtn" type="button">Go!</button>
										</span>
										<input type="text" class="form-control" id="searchID" name="form_num" placeholder="Search for form number..">
									</div>
								</div>
							<br/><br/>

						<!-- This is the Preview Form -->

        <div class="col-lg-12" style="padding-top: 10px;"></div>

				<!-- This is the Main Form -->

				<div class="col-md-6">

						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label text-info"><h3>Update Details</h3></label>
						</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Agent Name</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="agent_name2" id="agent_name2" readonly="readonly"/>
						</div>
					</div>


						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Call Date</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="input_date2" id="input_date2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Holder</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="plan_holder2" id="plan_holder2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Date of Birth</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="birth_date2" id="birth_date2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="address2" id="address2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Postcode</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="postcode2" id="postcode2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="telephone2" id="telephone2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Holder Email</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="planholder_email2" id="planholder_email2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Applicant</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="plan_applicant2" id="plan_applicant2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Relationship</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="applicant_relationship2" id="applicant_relationship2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="app_rel_address2" id="app_rel_address2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Postcode</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="app_rel_postcode2" id="app_rel_postcode2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
	                <input type="text" class="form-control" name="app_rel_tel2" id="app_rel_tel2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="app_rel_email2" id="app_rel_email2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Do you wish them to be notified of the funeral plan and the arrangements you have made?</label>
							<div class="col-md-6">
	                <input type="text" class="form-control" name="is_notified_arrangement2" id="is_notified_arrangement2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">My executors are</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="executors2" id="executors2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="executors_tel2" id="executors_tel2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Funeral Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="funeral_type2" id="funeral_type2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="funeral_plan_type2" id="funeral_plan_type2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Special Requests / Notes</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="funeral_plan_type2" id="funeral_plan_type2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Payment Option</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_option2" id="payment_option2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Full Payment Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_full_type_selected2" id="payment_full_type_selected2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Full Payment Amount</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_full_amount_selected2" id="payment_full_amount_selected2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay in 12-60 monthly instalments Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_installment_type_selected2" id="payment_installment_type_selected2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay in 12-60 monthly instalments Amount</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_installment_amount_selected2" id="payment_installment_amount_selected2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay by fixed monthly Age</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_fixed_age_selected2" id="payment_fixed_age_selected2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay by fixed monthly Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_fixed_type_selected2" id="payment_fixed_type_selected2" />
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay by fixed monthly Amount</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_fixed_monthly_selected2" id="payment_fixed_monthly_selected2" />
							</div>
						</div>

						<!-- <div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Assigned Status</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="status" id="status" />
							</div>
						</div> -->

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Comments</label>
							<div class="col-md-6">
								<label class="col-md-4 control-label" id="comments"></label>
								<textarea class="form-control" rows="5" name="comment2" id="comment2"></textarea>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Agent Special Request/Notes</label>
							<div class="col-md-6">
								<label class="col-md-4 control-label" id="comments"></label>
								<textarea class="form-control" rows="5" name="agent_special_note2" id="agent_special_note2" readonly="readonly"></textarea>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Set Status</label>
							<div class="col-md-6">
								<select class="form-control" name="status2" id="status2">
									<option value=""></option>
									<option value="Sale">Sale</option>
									<option value="Rejected">Rejected</option>
									<option value="Pending">Pending</option>
								</select>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Agent Action</label>
							<div class="col-md-6">
								  <input type="text" class="form-control" name="action2" id="action2" readonly="readonly"/>

							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Action</label>
							<div class="col-md-6">
								<select class="form-control" name="updated_action2" id="updated_action2">
									<option value=""></option>
									<!-- <option value="Successful Sale">Successful Sale</option>
									<option value="Rejected">Rejected</option>
									<option value="Pack-out">Pack-out</option>
									<option value="For callback">For callback</option>
									<option value="Pending">Pending</option> -->
								</select>
							</div>
						</div>

						<div class="form-group col-md-offset-2" id="successTransfer" style="display: none">
							<label class="col-md-4 control-label">Successful Transfer</label>
							<div class="col-md-6">
								<select class="form-control" name="success_transfer2" id="success_transfer2">
									<option value=""></option>
									<option value="Pending">Pending</option>
									<option value="Sale">Sale</option>
									<option value="Pack-out">Pack-out</option>
									<option value="Decline - Rejected">Decline - Rejected</option>
								</select>
							</div>
						</div>

						<div class="form-group col-md-offset-2" id="unSuccessTransfer" style="display: none">
							<label class="col-md-4 control-label">Unsuccessful Transfer</label>
							<div class="col-md-6">
								<select class="form-control" name="unsuccess_transfer2" id="unsuccess_transfer2">
									<option value=""></option>
									<option value="Decline (Rejected)">Decline (Rejected)</option>
									<option value="Callback">Callback</option>
									<option value="Pack-out (Mail)">Pack-out (Mail)</option>
								</select>
							</div>
						</div>



	          <input type="hidden" class="form-control" name="update_from" id="update_from" value="verifier" />

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>

					</form>
				</div>

				<div class="col-lg-6">
					<div class="form-horizontal">

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label text-danger"><h3>Original Details</h3></label>
						</div>

					<div class="form-group col-md-offset-2">
						<label class="col-md-4 control-label">Agent Name</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="agent_name" id="agent_name" readonly="readonly"/>
						</div>
					</div>


						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Call Date</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="input_date" id="input_date" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Holder</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="plan_holder" id="plan_holder" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Date of Birth</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="birth_date" id="birth_date" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="address" id="address" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Postcode</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="postcode" id="postcode" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="telephone" id="telephone" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Holder Email</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="planholder_email" id="planholder_email" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Applicant</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="plan_applicant" id="plan_applicant" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Relationship</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="applicant_relationship" id="applicant_relationship" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="app_rel_address" id="app_rel_address" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Postcode</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="app_rel_postcode" id="app_rel_postcode" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
	                <input type="text" class="form-control" name="app_rel_tel" id="app_rel_tel" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="app_rel_email" id="app_rel_email" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Do you wish them to be notified of the funeral plan and the arrangements you have made?</label>
							<div class="col-md-6">
	                <input type="text" class="form-control" name="is_notified_arrangement" id="is_notified_arrangement" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">My executors are</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="executors" id="executors" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="executors_tel" id="executors_tel" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Funeral Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="funeral_type" id="funeral_type" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Plan Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="funeral_plan_type" id="funeral_plan_type" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Special Requests / Notes</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="funeral_plan_type" id="funeral_plan_type" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Payment Option</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_option" id="payment_option" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Full Payment Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_full_type_selected" id="payment_full_type_selected" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Full Payment Amount</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_full_amount_selected" id="payment_full_amount_selected" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay in 12-60 monthly instalments Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_installment_type_selected" id="payment_installment_type_selected" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay in 12-60 monthly instalments Amount</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_installment_amount_selected" id="payment_installment_amount_selected" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay by fixed monthly Age</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_fixed_age_selected" id="payment_fixed_age_selected" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay by fixed monthly Type</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_fixed_type_selected" id="payment_fixed_type_selected" readonly="readonly"/>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Pay by fixed monthly Amount</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="payment_fixed_monthly_selected" id="payment_fixed_monthly_selected" readonly="readonly"/>
							</div>
						</div>

						<!-- <div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Assigned Status</label>
							<div class="col-md-6">
	              <input type="text" class="form-control" name="status" id="status" />
							</div>
						</div> -->

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Comments</label>
							<div class="col-md-6">
								<label class="col-md-4 control-label" id="comments"></label>
								<textarea class="form-control" rows="5" name="comment" id="comment" readonly="readonly"></textarea>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Agent Special Request/Notes</label>
							<div class="col-md-6">
								<label class="col-md-4 control-label" id="comments"></label>
								<textarea class="form-control" rows="5" name="agent_special_note" id="agent_special_note" readonly="readonly"></textarea>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Set Status</label>
							<div class="col-md-6">
								<select class="form-control" name="status" id="status" readonly="readonly">
									<option value=""></option>
									<option value="Sale">Sale</option>
									<option value="Rejected">Rejected</option>
									<option value="Pending">Pending</option>
								</select>
							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Agent Action</label>
							<div class="col-md-6">
								  <input type="text" class="form-control" name="action" id="action" readonly="readonly"/>

							</div>
						</div>

						<div class="form-group col-md-offset-2">
							<label class="col-md-4 control-label">Action</label>
							<div class="col-md-6">
								<select class="form-control" name="updated_action" id="updated_action" readonly="readonly">
									<option value=""></option>
									<!-- <option value="Successful Sale">Successful Sale</option>
									<option value="Rejected">Rejected</option>
									<option value="Pack-out">Pack-out</option>
									<option value="For callback">For callback</option>
									<option value="Pending">Pending</option> -->
								</select>
							</div>
						</div>

						<div class="form-group col-md-offset-2" id="successTransfer" style="display: none">
							<label class="col-md-4 control-label">Successful Transfer</label>
							<div class="col-md-6">
								<select class="form-control" name="success_transfer" id="success_transfer" readonly="readonly">
									<option value=""></option>
									<option value="Pending">Pending</option>
									<option value="Sale">Sale</option>
									<option value="Pack-out">Pack-out</option>
									<option value="Decline - Rejected">Decline - Rejected</option>
								</select>
							</div>
						</div>

						<div class="form-group col-md-offset-2" id="unSuccessTransfer" style="display: none">
							<label class="col-md-4 control-label">Unsuccessful Transfer</label>
							<div class="col-md-6">
								<select class="form-control" name="unsuccess_transfer" id="unsuccess_transfer" readonly="readonly">
									<option value=""></option>
									<option value="Decline (Rejected)">Decline (Rejected)</option>
									<option value="Callback">Callback</option>
									<option value="Pack-out (Mail)">Pack-out (Mail)</option>
								</select>
							</div>
						</div>

	          <input type="hidden" class="form-control" name="update_from" id="update_from" value="verifier" readonly="readonly"/>


					</div>
				</div>



					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('form-update')
<script>
jQuery('#input_date, #input_date2').datetimepicker({
  format:'Y-m-d H:i:s'
});

$("#status2").change(function() {
  var val = $("#status2").val();
	if(val == "Sale")
	{
		$('#updated_action2').hide();
	}
	else if(val == "Pending")
	{
		$('#updated_action2')
    .append('<option value="Pack-out Mail">Pack-out Mail</option>')
    .append('<option value="Pack-out Email">Pack-out Email</option>')
    .append('<option value="Callback">Callback</option>')
	}
	else if(val == "Rejected")
	{
		$('#updated_action2')
    .append('<option value="Not Interested">Not Interested</option>')
    .append('<option value="SRC">SRC</option>')
    .append('<option value="Rudeness/Possible Complaint">Rudeness/Possible Complaint</option>')
    .append('<option value="NFC">NFC</option>')
	}
	else
	{
		$('#updated_action2').show();
	}
});

// $("#action").change(function() {
//   var val = $("#action").val();
// 	if(val == "Successful Transfer")
// 	{
// 		$('#successTransfer').show();
// 		$('#unSuccessTransfer').hide();
// 	}
// 	else
// 	{
// 		$('#successTransfer').hide();
// 		$('#unSuccessTransfer').show();
// 	}
// });


$("#searchBtn").click(function() {
  var id = $("#searchID").val();
  $.ajax({
        url: "search-id",
        type: 'GET',
        data: {"id" : id},
        success: function(result){
        var myObj = $.parseJSON(result);
		if (myObj == undefined || myObj == null || myObj.length == 0)
		{
			alert("No result found.");
		}
		else
		{
			if(myObj[0][0] == undefined || myObj[0][0] == null || myObj[0][0].length == 0)
			{
				alert("Original data not found.");
			}
			else
			{
				$('#input_date').val(myObj[0][0].input_date);
				$('#plan_holder').val(myObj[0][0].plan_holder);
				$('#birth_date').val(myObj[0][0].birth_date);
				$('#address').val(myObj[0][0].address);
				$('#postcode').val(myObj[0][0].postcode);
				$('#telephone').val(myObj[0][0].telephone);
				$('#planholder_email').val(myObj[0][0].planholder_email);
				$('#plan_applicant').val(myObj[0][0].plan_applicant);
				$('#applicant_relationship').val(myObj[0][0].applicant_relationship);
				$('#app_rel_address').val(myObj[0][0].app_rel_address);
				$('#app_rel_postcode').val(myObj[0][0].app_rel_postcode);
				$('#app_rel_tel').val(myObj[0][0].app_rel_tel);
				$('#app_rel_email').val(myObj[0][0].app_rel_email);
				$('#is_notified_arrangement').val(myObj[0][0].is_notified_arrangement);
				$('#executors').val(myObj[0][0].executors);
				$('#executors_address').val(myObj[0][0].executors_address);
				$('#funeral_type').val(myObj[0][0].funeral_type);
				$('#executors_tel').val(myObj[0][0].executors_tel);
				$('#funeral_plan_type').val(myObj[0][0].funeral_plan_type);
				$('#funeral_details_notes').val(myObj[0][0].funeral_details_notes);
				$('#payment_option').val(myObj[0][0].payment_option);
				$('#payment_full_type_selected').val(myObj[0][0].payment_full_type_selected);
				$('#payment_full_amount_selected').val(myObj[0][0].payment_full_amount_selected);
				$('#payment_installment_type_selected').val(myObj[0][0].payment_installment_type_selected);
				$('#payment_installment_amount_selected').val(myObj[0][0].payment_installment_amount_selected);
				$('#payment_fixed_age_selected').val(myObj[0][0].payment_fixed_age_selected);
				$('#payment_fixed_type_selected').val(myObj[0][0].payment_fixed_type_selected);
				$('#payment_fixed_monthly_selected').val(myObj[0][0].payment_fixed_monthly_selected);
				$('#status').val(myObj[0][0].status);
				$('#comment').val(myObj[0][0].comment);
				$('#action').val(myObj[0][0].action);
				$('#agent_special_note').val(myObj[0][0].agent_special_note);
				$('#success_transfer').val(myObj[0][0].success_transfer);
				$('#unsuccess_transfer').val(myObj[0][0].unsuccess_transfer);
				$('#agent_name').val(myObj[0][0].name);
				$('#updated_action')
		    	.append('<option value="'+myObj[0][0].updated_action+'" selected>'+myObj[0][0].updated_action+'</option>');
			}

			if(myObj[1][0] != undefined || myObj[1][0] != null || myObj[1][0].length != 0)
			{
				$('#input_date2').val(myObj[1][0].input_date);
		        $('#plan_holder2').val(myObj[1][0].plan_holder);
		        $('#birth_date2').val(myObj[1][0].birth_date);
		        $('#address2').val(myObj[1][0].address);
		        $('#postcode2').val(myObj[1][0].postcode);
		        $('#telephone2').val(myObj[1][0].telephone);
		        $('#planholder_email2').val(myObj[1][0].planholder_email);
		        $('#plan_applicant2').val(myObj[1][0].plan_applicant);
		        $('#applicant_relationship2').val(myObj[1][0].applicant_relationship);
		        $('#app_rel_address2').val(myObj[1][0].app_rel_address);
		        $('#app_rel_postcode2').val(myObj[1][0].app_rel_postcode);
		        $('#app_rel_tel2').val(myObj[1][0].app_rel_tel);
		        $('#app_rel_email2').val(myObj[1][0].app_rel_email);
		        $('#is_notified_arrangement2').val(myObj[1][0].is_notified_arrangement);
		        $('#executors2').val(myObj[1][0].executors);
		        $('#executors_tel2').val(myObj[1][0].executors_tel);
		        $('#executors_address2').val(myObj[1][0].executors_address);
		        $('#funeral_type2').val(myObj[1][0].funeral_type);
		        $('#funeral_plan_type2').val(myObj[1][0].funeral_plan_type);
		        $('#funeral_details_notes2').val(myObj[1][0].funeral_details_notes);
		        $('#payment_option2').val(myObj[1][0].payment_option);
		        $('#payment_full_type_selected2').val(myObj[1][0].payment_full_type_selected);
		        $('#payment_full_amount_selected2').val(myObj[1][0].payment_full_amount_selected);
		        $('#payment_installment_type_selected2').val(myObj[1][0].payment_installment_type_selected);
		        $('#payment_installment_amount_selected2').val(myObj[1][0].payment_installment_amount_selected);
		        $('#payment_fixed_age_selected2').val(myObj[1][0].payment_fixed_age_selected);
		        $('#payment_fixed_type_selected2').val(myObj[1][0].payment_fixed_type_selected);
		        $('#payment_fixed_monthly_selected2').val(myObj[1][0].payment_fixed_monthly_selected);
		        $('#status2').val(myObj[1][0].status);
		        $('#comment2').val(myObj[1][0].comment);
		        $('#action2').val(myObj[1][0].action);
		        $('#agent_special_note2').val(myObj[1][0].agent_special_note);
		        $('#success_transfer2').val(myObj[1][0].success_transfer);
		        $('#unsuccess_transfer2').val(myObj[1][0].unsuccess_transfer);
		        $('#agent_name2').val(myObj[1][0].name);
				$('#updated_action2')
		    		.append('<option value="'+myObj[1][0].updated_action+'" selected>'+myObj[1][0].updated_action+'</option>');
				}

					// if(myObj[0].action == "Successful Transfer")
					// {
					// 	$('#successTransfer').show();
					// 	$('#unSuccessTransfer').hide();
					// }
					// else
					// {
					// 	$('#successTransfer').hide();
					// 	$('#unSuccessTransfer').show();
					// }
		}

    }});
});

</script>
@endsection

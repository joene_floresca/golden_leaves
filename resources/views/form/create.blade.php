@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Application Form</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					<form class="form-horizontal" role="form" method="POST" action="{{ url('form') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

            <p class="text-danger">Please complete as many details as you can, in BLOCK CAPITALS – if we require any further information we will contact you by phone.</p>

            <div class="panel panel-info">
              <div class="panel-heading text">Plan Holder’s Details</div>
              <div class="panel-body">

								<div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Form #</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="form_num" id="form_num" readonly="readonly" value="<?php echo '000'.$number; ?>">
                  </div>
                </div>

                <!-- <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label text-danger">Call Date *</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="input_date" id="input_date" required="required">
                  </div>
                </div> -->

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label text-danger">Plan Holder *</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="plan_holder" id="plan_holder" placeholder="Mr/Mrs/Ms/Other" required="required">
                  </div>
                </div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Date of birth</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="birth_date" id="birth_date" placeholder="">
                  </div>
                </div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label text-danger">Address *</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="address" id="address" placeholder="" required="required">
                  </div>
                </div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label text-danger">Postcode *</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="postcode" id="postcode" placeholder="" required="required">
                  </div>
                </div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label text-danger">Telephone *</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="telephone" id="telephone" placeholder="" required="required">
                  </div>
                </div>

								<div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Plan Holder Email</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="planholder_email" id="planholder_email" placeholder="">
                  </div>
                </div>

                <p class="text-danger"><strong>If you are applying for a plan for a third party please complete your details below:</strong</p>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Plan Applicant</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="plan_applicant" id="plan_applicant" placeholder="Mr/Mrs/Ms/Other">
                    </div>
                  </div>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Relationship</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="applicant_relationship" id="applicant_relationship" placeholder="">
                    </div>
                  </div>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Address</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="app_rel_address" id="app_rel_address" placeholder="">
                    </div>
                  </div>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Postcode</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="app_rel_postcode" id="app_rel_postcode" placeholder="">
                    </div>
                  </div>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Telephone</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="app_rel_tel" id="app_rel_tel" placeholder="">
                    </div>
                  </div>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="app_rel_email" id="app_rel_email" placeholder="">
                    </div>
                  </div>

                  <div class="form-group col-md-offset-2">
                    <label class="col-md-4 control-label">Do you wish them to be notified of the funeral plan and the arrangements you have made?</label>
                    <div class="col-md-6">
                      <select class="form-control" name="is_notified_arrangement" id="is_notified_arrangement">
                        <option value=""></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>

              </div>
            </div>

            <div class="panel panel-info">
              <div class="panel-heading">Important Contact Information</div>
              <div class="panel-body">

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">My executors are</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="executors" id="executors" placeholder="">
                  </div>
                </div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Telephone</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="executors_tel" id="executors_tel" placeholder="">
                  </div>
                </div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Address</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="executors_address" id="executors_address" placeholder="">
                  </div>
                </div>

              </div>
            </div>

            <div class="panel panel-info">
              <div class="panel-heading">Funeral Details</div>
              <div class="panel-body">

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Funeral Type</label>
                  <div class="col-md-6">
                    <select class="form-control" name="funeral_type" id="funeral_type">
                      <option value=""></option>
                      <option value="Cremation">Cremation</option>
                      <option value="Burial">Burial</option>
                    </select>
                  </div>
                </div>

                <div class="alert alert-warning alert-dismissable">
			              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                <p>Please note: The FROM50 Silver, Gold and Platinum plans include an allowance of <strong class="text-primary">£1100</strong> towards disbursement costs from the outset,
                    such as Doctor’s, Minister’s and cremation fees. Plans do not include the purchase of a grave. Included within the allowance is a service
                    at the crematorium or cemetery. An additional ceremony elsewhere, or any specific requirements at the crematorium, may incur an additional cost, which would be
                    payable at the time of the funeral.</p>
	            	</div>

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Plan Type</label>
                  <div class="col-md-6">
                    <select class="form-control" name="funeral_plan_type" id="funeral_plan_type">
                      <option value=""></option>
                      <option value="Bronze">Bronze</option>
                      <option value="Silver">Silver</option>
                      <option value="Gold">Gold</option>
                      <option value="Platinum">Platinum</option>
                    </select>
                  </div>
                </div>

                <div class="alert alert-warning alert-dismissable">
			              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                <p>We will recommend an independent funeral director from our network, that is local to you. If you wish to nominate your own please
                      confirm below. Golden Leaves will make every effort to allocate your plan to your selected funeral director, however this is not always
                      possible. In this event, we will contact you to advise you of this and discuss an alternative choice before processing your application.</p>
	            	</div>

                <!-- <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Special Requests / Notes</label>
                  <div class="col-md-6">
                    <textarea class="form-control" name="funeral_details_notes" id="funeral_details_notes" placeholder=""></textarea>
                  </div>
                </div> -->

                <div class="form-group col-md-offset-2">
                  <label class="col-md-4 control-label">Agent Special Request/Notes</label>
                  <div class="col-md-6">
                    <textarea class="form-control" name="agent_special_note" id="agent_special_note" placeholder=""></textarea>
                  </div>
                </div>

              </div>
           </div>


           <div class="panel panel-info">
             <div class="panel-heading">Our Plans - Payment Options</div>
             <div class="panel-body">
               <p>CHOOSE FROM PAYMENT IN FULL, 12-60 MONTHLY INSTALMENTS OR FIXED MONTHLY PAYMENTS</p>
               <div class="form-group col-md-offset-2">
                 <label class="col-md-4 control-label">Payment Option</label>
                 <div class="col-md-6">
                   <select class="form-control" name="payment_option" id="payment_option">
                     <option value=""></option>
                     <option value="Pay in full">Pay in full</option>
                     <option value="Pay in 12-60 monthly instalments">Pay in 12-60 monthly instalments</option>
                     <option value="Pay by fixed monthly payments">Pay by fixed monthly payments</option>
                   </select>
                 </div>
               </div>

               <div class="panel panel-info" style="display: none" id="payment_full_box">
                 <div class="panel-heading">Pay in full</div>
                 <div class="panel-body">
                    <div class="col-md-6">
                        <ul class="list-group">
                          <li class="">Pay a single payment by debit, credit card or cheque</li>
                          <li class="">Payments will be paid directly into the Golden Leaves Trust</li>
                          <li class="">If you cancel within 30 days we’ll refund the payment you’ve made</li>
                          <li class=" text-small">If you cancel after 30 days, we’ll refund the payment you’ve made, less the cancellation fee (see Terms and Conditions)</li>
                          <li class="">There are no medical questions and the plan is available to anyone.</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                      <p class="text-danger">*Regarding full payment and 12-60 monthly instalment options</p>
                      <div class="alert alert-warning alert-dismissable">
      			              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      			                <p>The plan prices indicated are standard prices available in most areas of the UK. In some areas, prices may vary. Also, if you are under 50, there
                            may be additional costs. We will advise you of
                            any variance before processing your application.</p>
      	            	</div>
                    </div>

                    <div class="form-group col-md-offset-2">
                      <label class="col-md-4 control-label">Select Type</label>
                      <div class="col-md-6">
                        <select class="form-control" name="payment_full_selected" id="payment_full_selected">
                          <option value=""></option>
                          <option value="Bronze">Bronze</option>
                          <option value="Silver">Silver</option>
                          <option value="Gold">Gold</option>
                          <option value="Platinum">Platinum</option>
                        </select>
                      </div>
                    </div>

										<div class="form-group col-md-offset-2">
                      <label class="col-md-4 control-label">Amount</label>
											<div class="col-md-6">

											<div class="input-group">
												  <span class="input-group-addon">£</span>
												  <input type="text" class="form-control" name="payment_full_amount_selected" id="payment_full_amount_selected" readonly="readonly">
												</div>
                    	</div>
										</div>


                 </div>
              </div>

              <div class="panel panel-info" style="display: none" id="payment_installment_box">
                <div class="panel-heading">Pay in 12-60 monthly instalments</div>
                <div class="panel-body">
                   <div class="col-md-12">
                       <ul class="list-group">
                         <li class="">Pay for your plan by Direct Debit over 12 months at no extra cost - See separate Direct Debit FORM No.1</li>
                         <li class="">Pay for your plan by Direct Debit over 24 – 60 Months with a deposit - See separate Direct Debit FORM No.3</li>
                         <li class="">Payments by 12-60 monthly instalments will be paid into the Golden Leaves Trust</li>
                         <li class="">If you cancel within 30 days we’ll refund the instalments you’ve made</li>
                         <li class="">If you cancel after 30 days, we’ll refund the instalments you’ve made, less the cancellation fee (see T&C’s)</li>
                         <li class="">If you die before all instalments have been made, the outstanding balance will be requested from your family or
                          estate. Alternatively the plan may be cancelled and we would refund any instalments made, less the cancellation
                          fee (see Terms & Conditions)</li>
                          <li class="">There are no medical questions and the plan is available to anyone</li>
                       </ul>
                   </div>

                   <p>The following  indicates your 12 monthly instalments depending on the plan you choose. </p>

									 <div class="form-group col-md-offset-2">
                     <label class="col-md-4 control-label"><strong>Silver</strong></label>
                     <div class="col-md-6">
											 <img src="{{ asset('images/silver.JPG') }}">
                     </div>
                   </div>

									 <div class="form-group col-md-offset-2">
                     <label class="col-md-4 control-label"><strong>Gold</strong></label>
                     <div class="col-md-6">
											 <img src="{{ asset('images/gold.JPG') }}">
                     </div>
                   </div>

									 <div class="form-group col-md-offset-2">
                     <label class="col-md-4 control-label"><strong>Platinum</strong></label>
                     <div class="col-md-6">
											 <img src="{{ asset('images/platinum.JPG') }}">
                     </div>
                   </div>

                   <div class="form-group col-md-offset-2">
                     <label class="col-md-4 control-label">Twelve payments of</label>
                     <div class="col-md-6">
                       <select class="form-control" name="payment_installment_type_selected" id="payment_installment_type_selected">
                         <option value=""></option>
                         <option value="Bronze">Bronze</option>
                         <option value="Silver">Silver</option>
                         <option value="Gold">Gold</option>
                         <option value="Platinum">Platinum</option>
                       </select>
                     </div>
                   </div>

									 <div class="form-group col-md-offset-2">
										 <label class="col-md-4 control-label">Amount</label>
										 <div class="col-md-6">

										 <div class="input-group">
												 <span class="input-group-addon">£</span>
												 <input type="text" class="form-control" name="payment_installment_amount_selected" id="payment_installment_amount_selected" readonly="readonly">
											 </div>
										 </div>
									 </div>


                </div>
             </div>

             <div class="panel panel-info" style="display: none" id="payment_fixed_box">
               <div class="panel-heading">Pay by fixed monthly payments</div>
               <div class="panel-body">

                  <p>Your funeral director’s fees are guaranteed to be covered after the first 12 months. Your funeral arrangements are
                  made with Golden Leaves who, in order to provide the benefits to you under your Plan, purchase a whole of life
                  insurance policy from AXA Wealth Ltd, trading as SunLife. This provides a sum of money to Golden Leaves in the
                  event of your death which they use to provide the benefits. Your Fixed Monthly Payments are made direct to AXA
                  Wealth Ltd to pay the premium on our behalf. You do not have any rights under the insurance they purchase. For
                  further information see the Terms & Conditions. </p>

                  <ul>
                    <li>You can pay by fixed monthly payments if you are a UK resident aged between 50 and 75. (See separate Direct Debit FORM No.2)</li>
                    <li>There are no medical questions asked and no medical underwriting is carried out.</li>
                    <li>Your Fixed Monthly Payments depend on your age and which funeral plan you choose.</li>
                    <li>Payments need to be made every month until you are aged 90, or until death – whichever is sooner.</li>
                    <li>Your monthly payments must be made by Direct Debit. (See separate Direct Debit FORM No.2)</li>
                    <li>During the first 12 months, full benefit will only be paid in the event of accidental death (see Terms & Conditions).
                      However, if your death is non-accidental, your nominated funeral Director could still carry out the funeral. Golden
                      Leaves would receive 120% of the money you have paid to help towards the cost of your funeral arrangements,
                      but your Next of Kin would need to pay the remaining balance.</li>
                    <li>If the total Fixed Monthly Payments you have paid in at the time of death, total more than the sum provided to
                      fund the funeral services from AXA Wealth Ltd, your estate will be eligible for a rebate of up to 100% of the
                      overpaid Fixed Monthly Payments from Golden Leaves.</li>
                    <li>If you cancel within 30 days, your payment will be refunded to you.</li>
                    <li>If you cancel after 30 days, or stop paying your payments, your funeral plan will be cancelled and you won’t receive anything back.</li>
                  </ul>

                  <p class="text-danger">The table below shows the fixed monthly payments for each plan</p>
                  <div class="col-md-8">
                    1. Refer to the column for your chosen plan (the plan names are at the top of the table). <br/>
                    2. Go to the row that indicates your current age. <br/>
                  </div>

                  <div class="col-md-4">
                    <blockquote>
                      If you need any assistance selecting your fixed monthly payment, please call us on 0800 85 44 48.
                    </blockquote>
                  </div>

                  <div class="col-md-12">
                    <img src="{{ asset('images/ref.JPG') }}">
                  </div>

                  <div class="col-md-12">
                    <br/>
                    <p class="text-danger">With a From50 funeral plan on a Fixed Monthly Payment option, you’ll benefit from:</p>
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <ul>
                            <li>Protection against rising funeral costs and nothing extra to pay for funeral director’s services – guaranteed</li>
                            <li>After 12 months, your plan will be fully guaranteed, providing you continue to meet your Fixed Monthly Payments. If you stop making your Fixed Monthly Payments, your plan will be cancelled and you will receive no refund</li>
                            <li>The benefit payment selected grows annually to help fight the effects of funeral inflation</li>
                            <li>Your Fixed Monthly Payments are guaranteed never to increase</li>
                            <li>No Fixed Monthly Payments to pay after age 90</li>
                            <li>Full cover starts only 12 months after you take out the Plan</li>
                            <li>Immediate accidental death cover*</li>
                            <li>Guaranteed acceptance - no medical health checks</li>
                            <li>If the total Fixed Monthly Payments you have paid in at the time of death, total more than the sum provided to fund the
                            funeral services from AXA Wealth Ltd, your estate will be eligible for a rebate of up to 100% of the overpaid Fixed Monthly
                            Payments from Golden Leaves*.</li>
                          </ul>
                    </div>
                  </div>

									<div class="form-group col-md-offset-2">
										<label class="col-md-4 control-label">Age</label>
										<div class="col-md-4">
											<select class="form-control" name="payment_fixed_age_selected" id="payment_fixed_age_selected">
												<option value=""></option>
												<option value="50">50</option>
												<option value="52">52</option>
												<option value="53">53</option>
												<option value="54">54</option>
												<option value="55">55</option>
												<option value="56">56</option>
												<option value="57">57</option>
												<option value="58">58</option>
												<option value="59">59</option>
												<option value="60">60</option>
												<option value="61">61</option>
												<option value="62">62</option>
												<option value="63">63</option>
												<option value="64">64</option>
												<option value="65">65</option>
												<option value="66">66</option>
												<option value="67">67</option>
												<option value="68">68</option>
												<option value="69">69</option>
												<option value="70">70</option>
												<option value="71">71</option>
												<option value="72">72</option>
												<option value="73">73</option>
												<option value="74">74</option>
												<option value="75">75</option>
											</select>
										</div>
									</div>

									<div class="form-group col-md-offset-2">
										<label class="col-md-4 control-label">Plan Type</label>
										<div class="col-md-4">
											<select class="form-control" name="payment_fixed_type_selected" id="payment_fixed_type_selected">
												<option value=""></option>
												<option value="FROM50 Bronze">FROM50 Bronze</option>
												<option value="FROM50 Silver">FROM50 Silver</option>
												<option value="FROM50 Gold">FROM50 Gold</option>
												<option value="FROM50 Platinum">FROM50 Platinum</option>
											</select>
										</div>
									</div>


									<div class="form-group col-md-offset-2">
										<label class="col-md-4 control-label">Monthly Payment</label>
										<div class="col-md-6">

										<div class="input-group">
												<span class="input-group-addon">£</span>
												<input type="text" class="form-control" name="payment_fixed_monthly_selected" id="payment_fixed_monthly_selected">
											</div>
										</div>
									</div>


               </div>
            </div>

            </div>
          </div>


										<div class="form-group col-md-offset-2">
											<label class="col-md-4 control-label">Initial Agent Action</label>
											<div class="col-md-6">
												<select class="form-control" name="action" id="action">
													<option value=""></option>
													<option value="Successful Sale">Successful Sale</option>
                          <option value="Pack-out mail">Pack-out mail</option>
                          <option value="Pack-out Email A">Pack-out Email A</option>
													<option value="Pack-out Email B">Pack-out Email B</option>
													<option value="For callback">For callback</option>
												</select>
											</div>
										</div>

										<!-- <div class="form-group col-md-offset-2" id="successTransfer" style="display: none">
											<label class="col-md-4 control-label">Successful Transfer</label>
											<div class="col-md-6">
												<select class="form-control" name="success_transfer" id="success_transfer">
													<option value=""></option>
													<option value="Pending - for confirmation to HUB">Pending - for confirmation to HUB</option>
													<option value="Sale - with GL's confirmation that the transfer was a successful sale">Sale - with GL's confirmation that the transfer was a successful sale</option>
													<option value="Pack out - with GL's confirmation (sent out forms and information)">Pack out - with GL's confirmation (sent out forms and information)</option>
													<option value="Decline - rejected">Decline - rejected</option>
												</select>
											</div>
										</div>

										<div class="form-group col-md-offset-2" id="unSuccessTransfer" style="display: none">
											<label class="col-md-4 control-label">Unsuccessful Transfer</label>
											<div class="col-md-6">
												<select class="form-control" name="unsuccess_transfer" id="unsuccess_transfer">
													<option value=""></option>
													<option value="Decline - rejected">Decline - rejected</option>
													<option value="For Agent Callback - Prospect requested a callback / Needs to disscuss with parter, daugther, son etc">For Agent Callback - Prospect requested a callback / Needs to disscuss with parter, daugther, son etc</option>
													<option value="For Senior Agent Callback - Re-pitch / Potential Sale / Requested a pack-out">For Senior Agent Callback - Re-pitch / Potential Sale / Requested a pack-out</option>
												</select>
											</div>
										</div> -->



						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('form-create')
<script>

// $("#action").change(function() {
//   var val = $("#action").val();
// 	if(val == "Successful Transfer")
// 	{
// 		$('#successTransfer').show();
// 		$('#unSuccessTransfer').hide();
// 	}
// 	else
// 	{
// 		$('#successTransfer').hide();
// 		$('#unSuccessTransfer').show();
// 	}
// });


jQuery('#input_date').datetimepicker({
  format:'Y-m-d H:i:s'
});

jQuery('#birth_date').datetimepicker({
  format:'Y-m-d',
  timepicker:false,
});

$("#payment_option").change(function() {
  var selected = $("#payment_option").val();
  console.log(selected);
  if(selected == "Pay in full")
  {
    $('#payment_full_box').show();
    $('#payment_installment_box').hide();
    $('#payment_fixed_box').hide();
  }
  else if(selected == "Pay in 12-60 monthly instalments")
  {
    $('#payment_installment_box').show();
    $('#payment_full_box').hide();
    $('#payment_fixed_box').hide();
  }
  else if(selected == "Pay by fixed monthly payments")
  {
    $('#payment_fixed_box').show();
    $('#payment_full_box').hide();
    $('#payment_installment_box').hide();
  }
  else
  {
    $('#payment_full_box').hide();
    $('#payment_installment_box').hide();
    $('#payment_fixed_box').hide();
  }

});


$("#payment_full_selected").change(function() {
  var selected = $("#payment_full_selected").val();
  if(selected == "Bronze")
	{
		$("#payment_full_amount_selected").val('2370');
	}
	else if(selected == "Silver")
	{
		$("#payment_full_amount_selected").val('3420');
	}
	else if(selected == "Gold")
	{
		$("#payment_full_amount_selected").val('3800');
	}
	else if(selected == "Platinum")
	{
		$("#payment_full_amount_selected").val('4120');
	}
	else
	{
		$("#payment_full_amount_selected").val('');
	}

});

$("#payment_installment_type_selected").change(function() {
  var selected = $("#payment_installment_type_selected").val();
  if(selected == "Bronze")
	{
		$("#payment_installment_amount_selected").val('197.50');
	}
	else if(selected == "Silver")
	{
		$("#payment_installment_amount_selected").val('285');
	}
	else if(selected == "Gold")
	{
		$("#payment_installment_amount_selected").val('316.67');
	}
	else if(selected == "Platinum")
	{
		$("#payment_installment_amount_selected").val('343.33');
	}
	else
	{
		$("#payment_installment_amount_selected").val('');
	}

});

</script>
@endsection

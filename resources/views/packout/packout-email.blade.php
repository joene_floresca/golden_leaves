@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Packout</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

			        <form class="form-horizontal" role="form" method="POST" action="{{ url('packout-email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">From: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email_from" id="email_from" value="info@futurechoices.co.uk" readonly="readonly" >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">To: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email_to" id="email_to" >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Pack-out: </label>
							<div class="col-md-6">
								<select class="form-control" name="packout_type" id="packout_type">
									<option value=""></option>
									<option value="A">A</option>
									<option value="B">B</option>
								</select>
							</div>
						</div>

						<div class="form-group" id="PackoutA" style="display:none">
							<label class="col-md-4 control-label">Body: </label>
							<div class="col-md-6">
								<textarea name="editor1" id="editor1" rows="10" cols="80">
					                <img class="img" src="{{ asset('images/email_temp.png') }}">
					                <p>Dear Mr. Customer,</p>
					                <p>Thank you for your interest in a pre-paid funeral plan. </p>


					                <p>We know the prospect of organizing funerals can be stressful; however Future Choices is 
					                	here to reduce that stress and burden to your family and friends and give you the best possible send off.
					                </p>
					                <p>
					                	We have attached a guide from our exclusive supplier Golden Leaves who have 30 years of experience in the funeral planning industry and have a choice of over 4000 funeral directors nationwide.
					                </p>
					                <p>
					                	We suggest that you read through the information attached and make an informed decision regarding your Plan. We will discuss this with you on our follow-up call.
					                </p>
					                <p>
					                	We will call you on the agreed date and time  __________________ to discuss any questions you have.
					                </p>
					                <p>
					                	If you don’t want us to contact you, you may unsubscribe on this link http://easyfuneralplans.com/#optoutlink
					                </p>
					                <p>
					                	Sales Team,
					                </p>
					                <p>
					                	Agent x
					                </p>

					            </textarea>
							</div>
						</div>
						<div class="form-group" id="PackoutB" style="display:none">
							<label class="col-md-4 control-label">Body: </label>
							<div class="col-md-6">
								<textarea name="editor2" id="editor2" rows="10" cols="80">
					                <img class="img" src="{{ asset('images/email_temp.png') }}">
					                <p>Dear X</p>
					                <p>It was a pleasure speaking to you today.</p>
					                <p>
					                	We know the prospect of organizing funerals can be stressful, however
										Future Choices is here to reduce that stress and burden to your family and friends and give you the best possible send off.
					                </p>
					                <p>
					                	We have attached a guide from our exclusive supplier Golden Leaves who have 30 years of experience in the funeral planning 
					                	industry and have a choice of over 4000 funeral directors nationwide.
					                </p>
					                <p>
					                	We suggest that you read through the information attached and make an informed decision regarding your Plan.
					                </p>
					                <p>
					                	Upon speaking to you it seems more appropriate to send you to the information regarding our ______ plan. 
					                	We will discuss this with you on our follow-up call.
					                </p>
					                <p>
					                	We will call you on ___________________ to discuss any questions you have.
					                </p>
					                <p>
					                	Sales Team,
					                </p>
					                <p>
					                	Agent x
					                </p>

					            </textarea>
							</div>
						</div>

						<div class="form-group" id="package_attach" style="display:none">
							<label class="col-md-4 control-label">Package (Attachment): </label>
							<div class="col-md-6">
								<select class="form-control" name="package_attachment" id="package_attachment">
									<option value=""></option>
									<option value="Silver">Silver</option>
									<option value="Gold">Gold</option>
									<option value="Platinum">Platinum</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>

						<!-- <iframe src="http://83.223.123.56:2095/cpsess81707558/3rdparty/squirrelmail/src/webmail.php" width="800" height="300"></iframe> -->


					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('packout-email')
<script>
CKEDITOR.replace('editor1');
CKEDITOR.replace('editor2');
$("#packout_type").change(function() {
  var type = $("#packout_type").val();
  switch(type)
  {
  	case 'A':
        $('#PackoutA').show();
        $('#PackoutB').hide();
        $('#package_attach').hide();
        break;
    case 'B':
    	$('#PackoutB').show();
    	$('#package_attach').show();
        $('#PackoutA').hide(); 
        break;
    default:
    	$('#PackoutB').hide();
        $('#PackoutA').hide();       
  }
});
</script>
@endsection

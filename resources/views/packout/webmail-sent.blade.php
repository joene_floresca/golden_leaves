@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Webmail Sent List</div>
				<div class="panel-body">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                          @if(Session::has('alert-' . $msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                          @endif
                        @endforeach
                        <table id="WebmailSent" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email From</th>
                                <th>Email To</th>
                                <th>Packout Type</th>
                                <th>Package Type</th>
                                <th>UK Time Sent</th>
                                <th>PH Time Sent</th>
                                <th>Sent by</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                    
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('packout-email-sent')
<script type="text/javascript">
    $(document).ready(function() {
        $('#WebmailSent').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'ajax-webmail-sent'
        });
    });
</script>
@endsection

/* NON-AJAX SCRITPS */
// $(document).ready(function() {
//  $('textarea').summernote();
// });
function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[0].cells.length;
		for(var i=0; i<colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[0].cells[i].innerHTML;
		}
}

function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	for(var i=0; i<rowCount; i++) {
		var row = table.rows[i];
		var chkbox = row.cells[0].childNodes[0];
		if(null != chkbox && true == chkbox.checked) {
			if(rowCount <= 1) {               // limit the user from removing all the fields
				alert("Cannot Remove all the Question.");
				break;
			}
			table.deleteRow(i);
			rowCount--;
			i--;
		}
	}
}

$("#orderCountry").change(function() {

  var client_raw = $('#orderClient').val();
  var country = $('#orderCountry').val();

	//set span text blank
	$('#oc_name').text("");

	//condition for country variable
	if (country.trim() != "") {
		country_text = country;
	}
	else {
		country_text = "(Country)";
	}

	var oc_name = "ORC_"+country_text+"_"+client_raw+"_";
	//$('#oc_name').text(oc_name);
	$('#orderOcNumber').val(oc_name);
	//alert($('#order_name_hidden').val());
});

$("#orderOcCount, #orderDeliveredBy").change(function() {
	var qty = $('#orderPoCount').val();
	var price = $('#orderAmount').val();
	var comm1 = $('#orderCommission').val();
	var sub_total = qty * price;
	var comm = sub_total * comm1;
	var sub_net = sub_total - comm;
	var total_decimal = parseFloat(sub_net).toFixed(2);
	var sub_total_decimal = parseFloat(sub_total).toFixed(2);
	$('#orderSubTotal').val(sub_total_decimal);
	$('#orderTotalValue').val(total_decimal);

});
